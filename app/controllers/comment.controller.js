//Import Model
const commentModel = require("../models/comment.model")
const mongoose = require("mongoose")
// Hàm tạo Drink
const createComment = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { id, postId, email, body,name } = req.body
    // B2 Kiểm tra dữ liệu
    if (!id) {
        return res.status(400).json({
            status: "Bad request",
            message: "id is required"
        })
    }
    if (!email) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is required"
        })
    }
    if (!postId) {
        return res.status(400).json({
            status: "Bad request",
            message: "postId is required"
        })
    }
    if (!body) {
        return res.status(400).json({
            status: "Bad request",
            message: "body is required"
        })
    }
    if (!name) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required"
        })
    }
    // B3 Xử lý 
    let newObj = {
        id,
        postId,
        email,
        body,name
    }
    try {
        const result = await commentModel.create(newObj);
        return res.status(201).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm Lấy dữ liệu tất cả Drink
const getAllComment = async (req, res) => {
    // B1 Thu thập dữ liệu
    const postId = req.query.postId
    // console.log(postId)
    let condition = {};
    if (postId) {
        condition.postId = postId;
    }
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await commentModel.find(condition);
    return res.status(200).json({
        result: result
    });

}
// Hàm Lấy dữ liệu tất cả Album
const getAllCommentOfPost = async (req, res) => {
    // B1 Thu thập dữ liệu
    const postId = req.params.postId
    if (postId) {


        // B2 Kiểm tra dữ liệu
        // B3 Xử lý 
        const result = await commentModel.find({
            postId: postId
        });
        return res.status(200).json({
            result
        });
    }
}
// Hàm lấy dữ liệu Drink bằng Id
const getCommentById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var commentId = req.params.commentId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Post Id is invalid"
        })
    }
    // B3 Xử lý 
    const result = await commentModel.findById(commentId);
    return res.status(200).json({
        result
    });
}
// Hàm Update Drink bằng Id
const updateCommentById = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { id, postId, email, body,name } = req.body
    var commentId = req.params.commentId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "drinkId is invalid"
        })
    }
    // B3 Xử lý 
    let updatedUser = {
        id,
        postId,
        email,
        body,name
    }
    const result = await commentModel.findByIdAndUpdate(commentId, updatedUser);
    return res.status(200).json({
        status: "Update User successfully",
        result
    });
}
// Hàm xóa Drink bằng Id
const deleteCommentById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var commentId = req.params.commentId
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await commentModel.findByIdAndDelete(commentId);
    return res.status(204).json({
        status: "Delete User successfully"
    });
}
// Export
module.exports = {
    createComment, getAllComment, getCommentById, updateCommentById, deleteCommentById, getAllCommentOfPost
}