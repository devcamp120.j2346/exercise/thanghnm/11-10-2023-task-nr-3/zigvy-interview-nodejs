// Import Model
const albumModel = require("../models/album.model")
const mongoose = require("mongoose")

// Hàm tạo Drink
const createAlbum = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { id, userId,title } = req.body
    // B2 Kiểm tra dữ liệu
    if (!id) {
        return res.status(400).json({
            status: "Bad request",
            message: "id is required"
        })
    }
    if (!userId) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required"
        })
    }
    if (!title) {
        return res.status(400).json({
            status: "Bad request",
            message: "username is required"
        })
    }

    
    // B3 Xử lý 
    let newAlbum = {
        id,
        userId,
        title
    }
    try {
        const result = await albumModel.create(newAlbum);
        return res.status(201).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
// Hàm Lấy dữ liệu tất cả Album
const getAllAlbum = async (req, res) => {
    // B1 Thu thập dữ liệu
    const userId = req.query.userId
    console.log(userId)
    let condition = {};
    if (userId) {
        condition.userId = userId;        
    }

    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await albumModel.find(condition);
    return res.status(200).json({
        result
    });

}
// Hàm Lấy dữ liệu tất cả Album
const getAllAlbumOfUser = async (req, res) => {
    // B1 Thu thập dữ liệu
    const userId = req.params.userId
    if(userId){

    
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await albumModel.find({
        userId:userId
    });
    return res.status(200).json({
        result
    });
    }
}
// Hàm lấy dữ liệu Album bằng Id
const getAlbumById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var albumId = req.params.albumId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "User Id is invalid"
        })
    }
    // B3 Xử lý 
    const result = await albumModel.findById(albumId);
    return res.status(200).json({
        result
    });

}
// Hàm Update Album bằng Id
const updateAlbumById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var albumId = req.params.albumId
    const { id, userId,title } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "albumId is invalid"
        })
    }
    // B3 Xử lý 
    let update = {
        id,
        userId,
        title
    }
    const result = await albumModel.findByIdAndUpdate(albumId, update);
    return res.status(200).json({
        status: "Update User successfully",
        result
    });

}
// Hàm xóa Drink bằng Id
const deleteAlbumById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var albumId = req.params.albumId
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await albumModel.findByIdAndDelete(albumId);
    return res.status(204).json({
        status: "Delete User successfully"
    });
}
// Export
module.exports = { createAlbum  ,getAllAlbum, getAlbumById, updateAlbumById, deleteAlbumById,getAllAlbumOfUser
}