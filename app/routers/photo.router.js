// Import express
const express = require("express");
const mongoose = require("mongoose");
const { getAllPhoto, createPhoto, getPhotoById, updatePhotoById, deletePhotoById } = require("../controllers/photo.controller");

const router = express.Router();
router.get("/", getAllPhoto);

router.post("/", createPhoto)

router.get("/:albumId", getPhotoById)

router.put("/:albumId", updatePhotoById)

router.delete("/:albumId", deletePhotoById)

module.exports = router;