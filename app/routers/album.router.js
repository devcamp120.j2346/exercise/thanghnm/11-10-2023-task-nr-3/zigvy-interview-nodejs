// Import express
const express = require("express");
const mongoose = require("mongoose")

const { createAlbum, getAllAlbum, getAlbumById, updateAlbumById, deleteAlbumById } = require("../controllers/album.controller");
const { getAllPhotoOfAlbum } = require("../controllers/photo.controller");
const router = express.Router();
router.get("/", getAllAlbum);

router.post("/", createAlbum)
router.get("/:albumId/photos",getAllPhotoOfAlbum )

router.get("/:albumId", getAlbumById)

router.put("/:albumId", updateAlbumById)

router.delete("/:albumId", deleteAlbumById)

module.exports = router;