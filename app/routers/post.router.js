// Import express
const express = require("express");
const mongoose = require("mongoose");
const { createPost, getAllPost, getPostById, updatePostById, deletePostById } = require("../controllers/post.controller");
const { getAllCommentOfPost } = require("../controllers/comment.controller");

const router = express.Router();
router.get("/", getAllPost);

router.post("/", createPost)
router.get("/:postId/comments",getAllCommentOfPost )

router.get("/:postId", getPostById)

router.put("/:postId", updatePostById)

router.delete("/:postId", deletePostById)

module.exports = router;