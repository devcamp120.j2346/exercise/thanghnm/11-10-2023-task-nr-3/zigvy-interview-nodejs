// Import express
const express = require("express")
const router = express.Router();

const { createUser, getAllUser, updateUserById, deleteUserById, getUserById,  } = require("../controllers/user.controller");
const { getAllAlbumOfUser } = require("../controllers/album.controller");
const { getAllTodosOfUser } = require("../controllers/todo.controller");
const { getAllPostOfUser } = require("../controllers/post.controller");

router.get("/", getAllUser);
router.post("/", createUser)
router.get("/:userId/albums",getAllAlbumOfUser )
router.get("/:userId/todos",getAllTodosOfUser )
router.get("/:userId/posts",getAllPostOfUser )

router.get("/:userId", getUserById)

router.put("/:userId", updateUserById)

router.delete("/:userId", deleteUserById)

module.exports = router;