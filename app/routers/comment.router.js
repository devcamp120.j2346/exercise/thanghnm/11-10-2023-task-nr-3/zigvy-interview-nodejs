// Import express
const express = require("express");
const mongoose = require("mongoose");
const { getAllComment, createComment, getCommentById, updateCommentById, deleteCommentById } = require("../controllers/comment.controller");

const router = express.Router();
router.get("/", getAllComment);

router.post("/", createComment)

router.get("/:postId", getCommentById)

router.put("/:postId", updateCommentById)

router.delete("/:postId", deleteCommentById)

module.exports = router;