//Khai báo mongoose
const mongoose = require("mongoose")
// Khai báo schema
const Schema = mongoose.Schema

const albumSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref:"Voucher"
    },
    id: {
        type: Number,
        required: true
        ,unique:true
    },
    title: {
        type: String,
        required: true
    }
});
module.exports = mongoose.model('Album', albumSchema);
